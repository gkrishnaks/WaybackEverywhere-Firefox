// Need to identify better way to handle persisting counts
// .. perhaps load old counts to session storage, then write to disk once every 4 minutes using alarms?
/*
async function storeCountstoStorage() {
  var countsChanged = false;
  if (oldcounts.archivedPageLoadsCount < counts.archivedPageLoadsCount) {
    oldcounts.archivedPageLoadsCount = counts.archivedPageLoadsCount;
    countsChanged = true;
  }
  if (oldcounts.waybackSavescount < counts.waybackSavescount) {
    oldcounts.waybackSavescount = counts.waybackSavescount;
    countsChanged = true;
  }
  const { oldCounts } = await browser.storage.session.get({
    oldCounts: {},
  });
  log("oldCounts from session " + JSON.stringify(oldCounts, null, 2));
  const {
    archivedPageLoadsCount: oldLoadsCount,
    waybackSavescount: oldSavesCount,
  } = oldCounts;
  if (
    typeof oldLoadsCount === "undefined" ||
    oldLoadsCount !== oldcounts.archivedPageLoadsCount ||
    oldSavesCount !== oldcounts.waybackSavescount
  ) {
    //  log(JSON.stringify(oldcounts));
    if (
      countsChanged &&
      (oldcounts.archivedPageLoadsCount > 0 || oldcounts.waybackSavescount > 0)
    ) {
      log("Setting counts to storage as " + JSON.stringify(oldcounts));
      await STORAGE.set({
        counts: oldcounts,
      });
      await browser.storage.session.set({ oldCounts: oldcounts });
    }
  } else {
    log(
      "Settings not stored to storage as they haven't chagned since last save. oldLoadsCount: " +
        oldLoadsCount +
        " oldSAvesCount " +
        oldSavesCount
    );
  }
}


// const Status = Object.freeze({
//   Idle: 0,
//   InProgress: 1,
//   Completed: 3,
//   Failed: 2,
// });
// var rulesStatus = { status: Status.InProgress }; // Intentionally set this to in-progress because when bg script starts after suspend, we want to setup rules only once.
// var rulesStatusProxy = new Proxy(rulesStatus, {
//   set: function(target, key, value) {
//     log(`${key} set to ${value}`);
//     target[key] = value;
//     return true;
//   },
// });
// //const sleep = (m) => new Promise((r) => setTimeout(r, m));
*/
