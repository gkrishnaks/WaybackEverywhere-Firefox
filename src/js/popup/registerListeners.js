/*******************************************************************************

    Wayback Everywhere - a browser addon/extension to redirect all pages to
    archive.org's Wayback Machine except the ones in Excludes List
    Copyright (C) 2018 - 2023 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/WaybackEverywhere-Firefox
*/

var PopupApp = window.PopupApp || {};

async function requestPermissions() {
  //in Windows, permission dialog renders underneath the extension popup making it unclickable.
  //Hide the popup page first only for windows.
  // This is an open bug in Firefox https://bugzilla.mozilla.org/show_bug.cgi?id=1798454

  if (PopupApp.os === "win") {
    setTimeout(window.close, 20);
    await browser.permissions.request({ origins: ["<all_urls>"] });
    return;
  }

  const granted = await browser.permissions.request({
    origins: ["<all_urls>"],
  });
  if (granted) {
    document.querySelector("#showpermissionsButton").style.display = "none";
    PopupApp.showPermissionRequestButton = false;
    chrome.tabs.reload({ bypassCache: true });
    setTimeout(window.close, 20);
  }
}

PopupApp.listenForClicks = (e) => {
  switch (e.target.id) {
    case "permissionsButton":
      requestPermissions();
      break;
    case "statsButton":
      PopupApp.statsShown = !PopupApp.statsShown;
      DOM.popupDOM.toggleStats(PopupApp.statsShown);
      break;
    case "enablebutton":
    case "disablebutton":
      PopupApp.toggleDisabled();
      break;
    case "savePagebutton":
      chrome.runtime.sendMessage(
        {
          type: "savetoWM",
          subtype: "fromPopup",
          url: PopupApp.currentUrl,
          tabid: PopupApp.tabid,
        },
        function(response) {
          PopupApp.log("returned to popup script" + response.message);
          window.close();
        }
      );
      break;
    case "firstArchivedButton":
      chrome.runtime.sendMessage(
        {
          type: "seeFirstVersion",
          subtype: "fromPopup",
          url: PopupApp.currentUrl,
          tabid: PopupApp.tabid,
        },
        (response) => {
          PopupApp.log(response);
          window.close();
        }
      );
      break;
    case "savePdfbutton":
      PopupApp.saveAsPDF();
      break;
    case "settingsbutton":
      PopupApp.openUrl(chrome.runtime.getURL("settings.html"));
      break;
    case "refreshbutton":
      PopupApp.openUrl(PopupApp.currentUrl);

      break;
    case "includeButton":
      PopupApp.removeSitefromexclude();
      break;
    case "tempIncludeButton":
      PopupApp.removeSitefromexcludeTemp();
      break;
    case "clearTempExcludeButton":
      PopupApp.clearTempRules("tempExcludes");
      break;
    case "applyTempToPermExcludesButton":
      PopupApp.applyTemptoPermanent("excludes");
      break;
    case "clearFromTempIncludesButton":
      PopupApp.clearTempRules("tempIncludes");
      break;
    case "tempExcludebutton":
      PopupApp.sendExcludeMessage("AddtoTempExcludesList");
      break;
    case "MainExcludesButton":
      PopupApp.sendExcludeMessage("AddtoExcludesList");
      break;
    case "applysitetoPermanentIncludesButton":
      PopupApp.applyTemptoPermanent("includes");
      break;
    case "openAllLinksButton":
      PopupApp.loadAll1pLinks(document.querySelector("#selectorInput").value);
      break;
    case "helpButton":
      PopupApp.openUrl(chrome.runtime.getURL("help.html"));
      break;
    case "waybackShareMsg":
      navigator
        .share({ url: PopupApp.currentUrl, text: PopupApp.pageTitle + "\n" })
        .catch(console.error);
      break;
    case "shareMsgLive":
      navigator
        .share({ url: PopupApp.liveURL, text: PopupApp.pageTitle + "\n" })
        .catch(console.error);
      break;
    case "shareliveURL":
      navigator.share({ url: PopupApp.liveURL }).catch(PopupApp.log);
      break;
    case "waybackShareURL":
      navigator.share({ url: PopupApp.currentUrl }).catch(PopupApp.log);
      break;
    /* case "rmrr":
        let nk = document.querySelector("#removethis");
        nk.style.display = "none"; // set to "" to display
        break; */
    default:
      PopupApp.log("click was not on a button");
  }
};

window.onload = () => {
  document.addEventListener("click", PopupApp.listenForClicks);
  let el = document.getElementById("selectorInput");
  if (!!el) {
    el.addEventListener("keyup", DOM.popupDOM.keyUpListener);
  }
};
